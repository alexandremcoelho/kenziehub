import "./App.css";
import { Pages } from "./components/pages";
import Bar from "./components/appbar";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Bar />
        <Pages />
      </header>
    </div>
  );
}

export default App;
