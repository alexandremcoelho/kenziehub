import { AppBar, Toolbar, MenuItem, Button } from "@material-ui/core/";
import { useHistory } from "react-router-dom";

const Bar = () => {
  const history = useHistory();

  const sendto = (path) => {
    history.push(path);
  };

  return (
    <AppBar>
      <Toolbar>
        <MenuItem onClick={() => sendto("/")}>login</MenuItem>
        <MenuItem onClick={() => sendto("/register")}>register</MenuItem>
        <MenuItem onClick={() => sendto("/home")}>home</MenuItem>
        <Button
          color="inherit"
          onClick={() => {
            sessionStorage.clear();
            sendto("/");
          }}
        >
          sair
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Bar;
