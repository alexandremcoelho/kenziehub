import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import "../../../../App.css";

import { Thech } from "./thech";

export const Skills = ({ parada, handleParada }) => {
  const [user, setUser] = useState("");

  const [token, setToken] = useState(() => {
    const sessionToken = sessionStorage.getItem("token") || null;
    return JSON.parse(sessionToken);
  });

  useEffect(() => {
    const options = {
      method: "GET",
      url: "https://kenziehub.me/profile",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .request(options)
      .then(function (response) {
        console.log(response.data);
        setUser(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, [parada]);

  return (
    <div className="components">
      {user &&
        user.techs.map((item, index) => <Thech key={index} name={item} />)}
    </div>
  );
};
