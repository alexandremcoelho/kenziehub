import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Skills } from "./skills";

export const Home = () => {
  const history = useHistory();
  const [token, setToken] = useState(() => {
    const sessionToken = sessionStorage.getItem("token") || null;
    return JSON.parse(sessionToken);
  });
  const [user, setUser] = useState(() => {
    const sessionUser = sessionStorage.getItem("user") || null;
    return JSON.parse(sessionUser);
  });
  const [parada, setparada] = useState(true);
  const handleParada = () => {
    setparada(!parada);
  };

  useEffect(() => {
    if (token === null) {
      history.push("/");
    }
  }, []);

  const schema = yup.object().shape({
    title: yup.string().required("obrigatório"),
  });

  const handleForm = (data) => {
    const options = {
      method: "POST",
      url: "https://kenziehub.me/users/techs",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    axios
      .request(options)
      .then(function (response) {
        console.log(response.data);
        handleParada();
      })
      .catch(function (error) {
        console.error(error);
      });
    // axios
    //   .post("https://kenziehub.me/users/techs", data, {
    //     Authorization: `Bearer ${token}`,
    //   })
    //   .then((response) => {
    //     reset();
    //   })
    //   .catch((e) => console.log(e));
  };
  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <>
      <div style={{ border: "3px solid black" }}>
        <form onSubmit={handleSubmit(handleForm)}>
          <div>
            <input placeholder="titulo" name="title" ref={register}></input>
            <p style={{ color: "#a22", fontSize: "12px" }}>
              {errors.title?.message}
            </p>
          </div>
          <Autocomplete
            id="combo-box-demo"
            options={statusOptions}
            getOptionLabel={(option) => option.status}
            renderInput={(params) => (
              <TextField
                {...params}
                margin="normal"
                variant="outlined"
                label="Level"
                name="status"
                size="small"
                color="primary"
                inputRef={register}
                error={!!errors.status}
                helperText={errors.status?.message}
              />
            )}
          />
          <div>
            <button type="submit">enviar</button>
          </div>
        </form>
      </div>

      <Skills parada={parada} handleParada={handleParada}></Skills>
    </>
  );
};

const statusOptions = [
  { status: "Iniciante" },
  { status: "Intermediário" },
  { status: "Avançado" },
];
