import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { useHistory } from "react-router-dom";

export const Body = () => {
  const history = useHistory();
  const schema = yup.object().shape({
    email: yup.string().email("email invalido").required("obrigatório"),
    password: yup
      .string()
      .min(8, "minimo de 8 caracteres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "senha fraca"
      )
      .required("obrigatório"),
    name: yup.string().required("obrigatorio"),
    bio: yup.string().required("obrigatorio"),
    contact: yup.string().required("obrigatorio"),
    course_module: yup.string().required("obrigatorio"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    console.log(data);
    axios
      .post("https://kenziehub.me/users", data)
      .then((response) => {
        reset();
        history.push("/");
      })
      .catch((e) => console.log(e));
  };

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <div>
        <input placeholder="email" name="email" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.email?.message}
        </p>
      </div>
      <div>
        <input placeholder="senha" name="password" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.password?.message}
        </p>
      </div>
      <div>
        <input placeholder="nome" name="name" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.name?.message}
        </p>
      </div>
      <div>
        <input placeholder="bio" name="bio" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>{errors.bio?.message}</p>
      </div>
      <div>
        <input placeholder="contato" name="contact" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.contact?.message}
        </p>
      </div>
      <div>
        <input placeholder="modulo" name="course_module" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.course_module?.message}
        </p>
      </div>

      <div>
        <button type="submit">enviar</button>
      </div>
    </form>
  );
};
