import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { useHistory } from "react-router-dom";

export const Log = () => {
  const history = useHistory();
  const schema = yup.object().shape({
    email: yup.string().email("email invalido").required("obrigatório"),
    password: yup
      .string()
      .min(8, "minimo de 8 caracteres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "senha fraca"
      )
      .required("obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    console.log(data);
    axios
      .post("https://kenziehub.me/sessions", data)
      .then((response) => {
        console.log(response.data.user);
        console.log(response.data.token);
        sessionStorage.clear();
        sessionStorage.setItem("token", JSON.stringify(response.data.token));
        sessionStorage.setItem("user", JSON.stringify(response.data.user));
        reset();
        history.push("/home");
      })
      .catch((e) => console.log(e));
  };

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <div>
        <input placeholder="email" name="email" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.email?.message}
        </p>
      </div>
      <div>
        <input placeholder="senha" name="password" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.password?.message}
        </p>
      </div>

      <div>
        <button type="submit">enviar</button>
      </div>
    </form>
  );
};
