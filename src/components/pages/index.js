import { Switch, Route } from "react-router-dom";
import { Body } from "./reg";
import { Log } from "./login";
import { Home } from "./home";

export const Pages = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Log />
      </Route>
      <Route path="/register">
        <Body />
      </Route>
      <Route path="/home">
        <Home />
      </Route>
    </Switch>
  );
};
